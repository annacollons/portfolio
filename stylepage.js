// functions for first slide show
var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}

// funtions for second slide show
var slideIndexTwo = 1;
showDivsTwo(slideIndexTwo);

function plusDivsTwo(n) {
  showDivsTwo(slideIndexTwo += n);
}

function showDivsTwo(n) {
  var i;
  var x = document.getElementsByClassName("mySlidesTwo");
  if (n > x.length) {slideIndexTwo = 1}    
  if (n < 1) {slideIndexTwo = x.length}
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";  
  }
  x[slideIndexTwo-1].style.display = "block";  
}